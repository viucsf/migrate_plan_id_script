package com.amazonaws.samples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class UpdateExistingPlan {

	protected static void updatePlanWithExistingRecords(){
		try {
			String fileName = "./existing_migration_result.txt";
			File file = new File(fileName);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){			   
				String[] current_state = line.split(" ");
				String offerId=current_state[0];
				String planId = current_state[current_state.length - 1];
				String status=UpdatePlanId.updatePlanId(offerId, planId);
				System.out.println("status "+status);
			}												
		} catch (Exception e) {			
			System.out.println("Failed to open File");
		}
	}
}
