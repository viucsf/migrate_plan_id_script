package com.amazonaws.samples;

import org.json.JSONException;
import org.json.JSONObject;

public class FetchPlanId {
	protected static String getPlanIdByPrivileges(JSONObject privileges) throws JSONException {			
		if(privileges.get("adsPrivilege").equals("SHOW_ADS")) {
			if(privileges.get("contentPrivilege").equals("PREMIUM_GRANTED"))
				return "PremiumwithAds";
			else
				return "Basic";
		}
		else 
			return "Premium";			
	}
}