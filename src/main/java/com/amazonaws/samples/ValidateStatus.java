package com.amazonaws.samples;

import java.io.IOException;

public class ValidateStatus extends Application {

	protected static void updateMigrationStatus(String migration_status) {
		try {	        	
			migration_result_handle.write(migration_status.getBytes(), 0, migration_status.length());			
	    } 
		catch (IOException e) {
	    	e.printStackTrace();
	    } 
	}
	protected static void showCurrentMigration(String existingRecords) {
		try {	        	
			existing_migration_result_handle.write(existingRecords.getBytes(), 0, existingRecords.length());			
	    } 
		catch (IOException e) {
	    	e.printStackTrace();
	    } 
	}
}