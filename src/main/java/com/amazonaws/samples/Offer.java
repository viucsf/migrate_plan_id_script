package com.amazonaws.samples;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONObject;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;

public class Offer extends Application{

	private static String offerId;

	static void getAllOffers() {		
		try {						
			ScanSpec scanSpec = new ScanSpec().withProjectionExpression("offerId,planId,#privileges")
					.withNameMap(new NameMap().with("#privileges", "privileges"));
			ItemCollection<ScanOutcome> items = table.scan(scanSpec);
			Iterator<Item> iter = items.iterator();			
			
			migration_result_handle = new FileOutputStream(new File("./migration_result.txt"));
			existing_migration_result_handle = new FileOutputStream(new File("./existing_migration_result.txt"));
								
			while (iter.hasNext() ) {
				try
				{
					Item item = iter.next();
					Date date = new Date();							
					JSONObject privileges = new JSONObject(item.get("privileges").toString());
					
					offerId= (String) item.get("offerId");		
					String existingPlanId= (String) item.get("planId");	
					if(existingPlanId!=null) {
						String existingRecords=offerId+" "+privileges+ " "+existingPlanId+"\n";		
						ValidateStatus.showCurrentMigration(existingRecords);
					}
										
					String planId = FetchPlanId.getPlanIdByPrivileges(privileges);		
					String status=UpdatePlanId.updatePlanId(offerId,planId);		
					String migration_status=offerId+" "+privileges+ " "+planId+" "+date.toString()+" "+status+"\n";											
					ValidateStatus.updateMigrationStatus(migration_status);
				}
				catch (Exception e) {			
					System.out.println("getOffers failed for offerId "+offerId+" Error " +e.getMessage());					
				}					
			}
		} catch (Exception e) {			
			System.out.println("Failed to fetch OfferBundle from the table");					
		}
		
	}

}
