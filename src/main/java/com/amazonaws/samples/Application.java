package com.amazonaws.samples;

import java.io.OutputStream;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;

public class Application {
	
	static String tableName;			
	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);
	static Table table;
	static OutputStream migration_result_handle =null;
	static OutputStream existing_migration_result_handle = null;
		
	public static void main(String[] args) throws Exception {
		tableName="ofr."+args[0]+".Offers";
		table = dynamoDB.getTable(tableName);
		System.out.println("Table Name : "+tableName);
		Offer.getAllOffers();					
	}	
}