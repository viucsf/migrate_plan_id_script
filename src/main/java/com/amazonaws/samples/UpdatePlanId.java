package com.amazonaws.samples;

import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;

public class UpdatePlanId extends Application{
	protected static String updatePlanId(String offerId,String planId) {
		UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("offerId", offerId)
				.withUpdateExpression("set planId = :planId")
				.withValueMap(new ValueMap().withString(":planId", planId));	
		try {
				@SuppressWarnings("unused")
				UpdateItemOutcome outcome = table.updateItem(updateItemSpec);		
				System.out.println("Plan Id updated successfully for "+offerId);
		        return "SUCCESS";		     
	    	}
		catch (Exception e) {
				System.out.println("update planId failed for offerId "+offerId+" Error "+e.getMessage());
	            return "FAILED";
	        }	
	}	
}
