# README #

Migrate_Plan_id_Script

### What is this repository for? ###

* This repository is for to run a one time script to push PlanId into database
* There are main 3 types of Plans in Phase 1.
Basic, PremiumwithAds and Premium.
* This Script will run and update the field PlanId in the database based on resective privileges.

### How do I get set up? ###

* Import the project in Eclipse.
* Config AWS Crendentials.
* Run Java Local Server.

### How to Run Jar? ###

* Go to the Folder Structure.
* Run jar by command java -jar <JAR_NAME> <e2e/prd>
* Pass the argument in command line e2e or prd 

